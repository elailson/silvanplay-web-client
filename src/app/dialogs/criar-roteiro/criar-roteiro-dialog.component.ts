import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { LocalStorageService } from "angular-web-storage";
import { ToasterService } from 'angular2-toaster';
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Roteiro } from 'src/app/model/Roteiro';
import { Solicitacao } from 'src/app/model/Solicitacao';
import { RoteiroService } from 'src/app/services/roteiro.service';

export interface DialogData {
    metodo: any;
    solicitacao: Solicitacao;
}

@Component({
    selector: 'app-criar-roteiro-dialog',
    templateUrl: 'criar-roteiro-dialog.component.html',
    styleUrls: ['criar-roteiro-dialog.component.css']
})
export class CriarRoteiroDialogComponent {

    @BlockUI() blockUI: NgBlockUI;

    formRoteiro = this.formBuilder.group({
        nome: new FormControl('', Validators.required),
        campus: new FormControl('', Validators.required),
        roteiro: new FormControl('', Validators.required),
        turno: new FormControl('', Validators.required),
        trajeto: new FormControl('', Validators.required),
        horario: new FormControl(''),
        horaSaidaManha: new FormControl(''),
        horaVoltaManha: new FormControl(''),
        horaSaidaTarde: new FormControl(''),
        horaVoltaTarde: new FormControl('')
    });

    constructor(
        private dialogRef: MatDialogRef<CriarRoteiroDialogComponent>,
        private formBuilder: FormBuilder,
        private roteiroService: RoteiroService,
        private toasterService: ToasterService,
        @Inject(MAT_DIALOG_DATA) private data: DialogData) { }

    save() {
        let roteiro = new Roteiro().buildObjectFromForm(this.formRoteiro);
        console.log(roteiro);
        if (this.formRoteiro.valid) {
            this.blockUI.start('Salvando roteiro...');
            let roteiro = new Roteiro().buildObjectFromForm(this.formRoteiro);
            this.roteiroService.save(roteiro)
                .subscribe(retorno => {
                    this.toasterService.pop('success', 'Salvo com sucesso!');
                    this.blockUI.stop();
                    this.cancelar();
                });
        }
    }

    cancelar(): void {
        this.dialogRef.close();
    }
}