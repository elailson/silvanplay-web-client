import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastraInstituicaoEducacionalComponent } from './pages/cadastra-instituicao-educacional/cadastra-instituicao-educacional.component';
import { LoginComponent } from './pages/login/login.component';
import { CadastraTelefoneComponent } from './pages/cadastra-telefone/cadastra-telefone.component';
import { GerenciaSolicitacaoComponent } from './pages/gerencia-solicitacao/gerencia-solicitacao.component';
import { AuthGuardEducacao } from './auth/guards/auth.guardEducacao';
import { HomeComponent } from './pages/home/home.component';
import { CadastraAdministradorComponent } from './pages/cadastra-administrador/cadastra-administrador.component';
import { AuthGuardMaster } from './auth/guards/auth.guardMaster';
import { GerenciaAcademicoComponent } from './pages/gerencia-academico/gerencia-academico.component';
import { GerenciaRoteiroComponent } from './pages/gerencia-roteiro/gerencia-roteiro.component';
import { AuthGuardSolicitacao } from './auth/guards/auth.guardSolicitacao';
import { AuthGuardLogado } from './auth/guards/auth.guardLogado';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardLogado] },
  { path: 'cadastra-instituicao', component: CadastraInstituicaoEducacionalComponent, canActivate: [AuthGuardEducacao] },
  { path: 'gerencia-solicitacoes', component: GerenciaSolicitacaoComponent, canActivate: [AuthGuardSolicitacao] },
  { path: 'gerencia-academico', component: GerenciaAcademicoComponent, canActivate: [AuthGuardEducacao] },
  { path: 'gerencia-roteiro', component: GerenciaRoteiroComponent, canActivate: [AuthGuardEducacao] },
  { path: 'cadastra-administrador', component: CadastraAdministradorComponent, canActivate: [AuthGuardMaster] },
  { path: 'cadastra-telefone', component: CadastraTelefoneComponent, canActivate: [AuthGuardSolicitacao] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
