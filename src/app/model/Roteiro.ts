import { FormGroup } from '@angular/forms';

export class Roteiro {
    id: number;
    nome: string;
    campus: string;
    roteiro: number;
    turno: number;
    trajeto: string;
    horario: string;
    assentosDisponiveis: number;
    horaSaidaManha: string;
    horaVoltaManha: string;
    horaSaidaTarde: string;
    horaVoltaTarde: string;

    buildObjectFromForm(form: FormGroup): Roteiro {
        this.nome = form.get('nome').value;
        this.campus = form.get('campus').value;
        this.roteiro = form.get('roteiro').value;
        this.turno = form.get('turno').value;
        this.trajeto = form.get('trajeto').value;
        this.horario = form.get('horario').value;
        this.horaSaidaManha = form.get('horaSaidaManha').value;
        this.horaVoltaManha = form.get('horaVoltaManha').value;
        this.horaSaidaTarde = form.get('horaSaidaTarde').value;
        this.horaVoltaTarde = form.get('horaVoltaTarde').value;

        return this;
    }
}