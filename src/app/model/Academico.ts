export class Academico {
    id: number;
    nome: string;
    cpf: string;
    dataNascimento: string;
    telefone: string;
    sexo: number;
    raca: number;
    deficiencia: number;
    email: string;
    isCadastrado: boolean;
    roteiro: number;
    curso: string;
    turno: number;
    nivelCurso: number;
    semestreAtual: number;
    isBolsista: number;
    programaBolsa: string;
    percentualBolsa: number;
    enderecoId: number;
    roteiroId: number;
    dataCriacao: string;
}