import { FormGroup } from '@angular/forms';

export class Telefone {

    nome: string;
    telefone1: string;
    telefone2: string;
    telefone3: string;

    constructor(telefone: FormGroup) {
        this.nome = telefone.get('nome').value.toLocaleUpperCase();
    }
}