import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';

export class AuthGuardLogado implements CanActivate {

    constructor(private router: Router,
        private localStorageService: LocalStorageService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const usuario = this.localStorageService.get('usuario');
        if (!usuario) {
            this.router.navigate(['login']);
            return false;
        }

        return true;
    }
}