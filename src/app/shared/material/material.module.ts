// import { NgModule } from '@angular/core';
// import { MatStepperModule } from '@angular/material/stepper';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MatSelectModule } from '@angular/material/select';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatCardModule } from '@angular/material/card';
// import { MatMenuModule } from '@angular/material/menu';
// import { MatTooltipModule } from '@angular/material/tooltip';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatButtonModule } from '@angular/material/button';
// import { MatCheckboxModule } from '@angular/material/checkbox';
// import { MatInputModule } from '@angular/material/input';
// import { MatTableModule } from '@angular/material/table';
// import { MatAutocompleteModule } from '@angular/material/autocomplete';
// import { MatIconModule } from '@angular/material/icon';
// import { MatPaginatorModule } from '@angular/material/paginator';
// import { MatExpansionModule } from '@angular/material/expansion';
// import { MatSortModule } from '@angular/material/sort';

// @NgModule({
//     imports: [
//         MatButtonModule,
//         MatCheckboxModule,
//         MatStepperModule,
//         BrowserAnimationsModule,
//         FormsModule,
//         ReactiveFormsModule,
//         MatFormFieldModule,
//         MatInputModule,
//         MatSelectModule,
//         MatRadioModule,
//         MatCardModule,
//         MatTooltipModule,
//         MatMenuModule,
//         MatAutocompleteModule,
//         MatIconModule,
//         MatPaginatorModule,
//         MatDatepickerModule,
//         MatExpansionModule,
//         MatSortModule
//     ],
//     exports: [
//         MatButtonModule,
//         MatCheckboxModule,
//         MatStepperModule,
//         BrowserAnimationsModule,
//         FormsModule,
//         ReactiveFormsModule,
//         MatFormFieldModule,
//         MatInputModule,
//         MatSelectModule,
//         MatRadioModule,
//         MatCardModule,
//         MatTableModule,
//         MatTooltipModule,
//         MatMenuModule,
//         MatAutocompleteModule,
//         MatIconModule,
//         MatPaginatorModule,
//         MatDatepickerModule,
//         MatExpansionModule,
//         MatSortModule
//     ]
// })
// export class MaterialModule { }