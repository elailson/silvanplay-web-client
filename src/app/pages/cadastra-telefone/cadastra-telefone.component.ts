import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CadastroTelefoneService } from 'src/app/services/cadastra-telefone.service';
import { ToasterService } from 'angular2-toaster';
import { LocalStorageService } from 'angular-web-storage';
import { Telefone } from 'src/app/model/Telefone';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastra-telefone',
  templateUrl: './cadastra-telefone.component.html',
  styleUrls: ['./cadastra-telefone.component.css']
})
export class CadastraTelefoneComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  @ViewChild('maskedTel1', { static: false }) telefone1: ElementRef;
  @ViewChild('maskedTel2', { static: false }) telefone2: ElementRef;
  @ViewChild('maskedTel3', { static: false }) telefone3: ElementRef;

  telefone: FormGroup;
  maskTel1: string;
  maskTel2: string;
  maskTel3: string;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private cadastraTelefoneService: CadastroTelefoneService,
    private toasterService: ToasterService,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    this.telefone = this.formBuilder.group({
      nome: new FormControl('', Validators.required),
      telefone1: new FormControl('', Validators.required),
      telefone2: new FormControl(''),
      telefone3: new FormControl('')
    });
  }

  cadastrar() {
    if (this.validaForm()) {
      this.blockUI.start('Cadastrando');

      let objTelefone = new Telefone(this.telefone);
      objTelefone.telefone1 = this.telefone1.nativeElement.value;
      objTelefone.telefone2 = this.telefone2.nativeElement.value;
      objTelefone.telefone3 = this.telefone3.nativeElement.value;

      this.cadastraTelefoneService.cadastrar(objTelefone)
        .subscribe(retorno => {
          this.toasterService.pop('success', 'Cadastrado com sucesso!');
          this.telefone.reset();
          this.blockUI.stop();
        }, error => {
          this.toasterService.pop('error', 'Erro ao cadastrar: ' + error);
          this.blockUI.stop();
        });
    } else {
      this.toasterService.pop('warning', 'Preencha os campos obrigatórios');
    }
  }

  validaForm(): boolean {
    let nome = this.telefone.get('nome').value;
    let telefone1 = this.telefone.get('telefone1').value;

    if (nome === '' || nome === null || nome === undefined) {
      this.telefone.get('nome').hasError('required');
      return false;
    } else if (telefone1 === '' || telefone1 === null || telefone1 === undefined) {
      this.telefone.get('telefone1').setErrors(null);
      return false;
    }

    return true;
  }

  changeMask(num: number) {
    switch (num) {
      case 1:
        if (this.telefone.get('telefone1').value.length <= 3) {
          this.maskTel1 = '0000';
        } else if (this.telefone.get('telefone1').value.length > 3 && this.telefone.get('telefone1').value.length <= 10) {
          this.maskTel1 = '(00) 0000-00000';
        } else if (this.telefone.get('telefone1').value.length > 10) {
          this.maskTel1 = '(00) 0 0000-0000';
        }
        break;
      case 2:
        if (this.telefone.get('telefone2').value.length <= 3) {
          this.maskTel2 = '0000';
        }
        if (this.telefone.get('telefone2').value.length > 3 && this.telefone.get('telefone2').value.length <= 10) {
          this.maskTel2 = '(00) 0000-00000';
        }
        if (this.telefone.get('telefone2').value.length > 10) {
          this.maskTel2 = '(00) 0 0000-0000';
        }
        break;
      case 3:
        if (this.telefone.get('telefone3').value.length <= 3) {
          this.maskTel3 = '0000';
        }
        if (this.telefone.get('telefone3').value.length > 3 && this.telefone.get('telefone3').value.length <= 10) {
          this.maskTel3 = '(00) 0000-00000';
        }
        if (this.telefone.get('telefone3').value.length > 10) {
          this.maskTel3 = '(00) 0 0000-0000';
        }
        break;
    }
  }
}
