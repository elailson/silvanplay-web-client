import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ToasterService } from 'angular2-toaster';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatSort } from '@angular/material/sort';
import { AcademicoService } from 'src/app/services/academico.service';
import { Academico } from 'src/app/model/Academico';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-gerencia-academico',
  templateUrl: './gerencia-academico.component.html',
  styleUrls: ['./gerencia-academico.component.css']
})
export class GerenciaAcademicoComponent implements OnInit {

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @BlockUI() blockUI: NgBlockUI;
  dataSource: MatTableDataSource<Academico>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'nome', 'email', 'roteiro', 'curso', 'turno'];

  constructor(private academicoService: AcademicoService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.blockUI.start('Buscando acadêmicos');
    this.academicoService.findAll()
      .subscribe(academicos => {
        this.dataSource = new MatTableDataSource(academicos);
        this.dataSource.paginator = this.paginator;
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('warning', 'Erro ao buscar acadêmicos')
        this.blockUI.stop();
      });
  }

}
