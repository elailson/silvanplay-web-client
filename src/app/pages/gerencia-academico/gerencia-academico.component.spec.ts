import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciaAcademicoComponent } from './gerencia-academico.component';

describe('GerenciaSolicitacaoComponent', () => {
  let component: GerenciaAcademicoComponent;
  let fixture: ComponentFixture<GerenciaAcademicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciaAcademicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciaAcademicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
