import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { AdminEnum } from 'src/app/enum/AdminEnum';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

@Injectable()
export class HeaderComponent implements OnInit {

  usuario: string;

  perfilEducacao: boolean = false;
  perfilSolicitacao: boolean = false;
  perfilMaster: boolean = false;
  perfilTelefone: boolean = false;

  constructor(private router: Router,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    this.usuario = this.localStorage.get('usuario');
    let permissoes = this.localStorage.get('permissoes');
    if (permissoes) {
      this.setaPerfis(permissoes);
    }
  }

  telaNavigate(tela: string) {
    this.router.navigate([tela]);
  }

  setaPerfis(permissoes: Array<string>) {
    permissoes.forEach(permissao => {
      switch (permissao.trim()) {
        case AdminEnum.EDUCACAO:
          this.perfilEducacao = true;
          break;
        case AdminEnum.GERENCIA_SOLICITACAO:
          this.perfilSolicitacao = true;
          break;
        case AdminEnum.ADM_MASTER:
          this.perfilMaster = true;
          break;
        case AdminEnum.TELEFONE:
          this.perfilTelefone = true;
          break;
      }
    });
  }

  sair() {
    this.localStorage.clear();
  }
}
