import { Component, OnInit, ViewChild } from '@angular/core';
import { LocalStorageService } from 'angular-web-storage';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BaseChartDirective } from 'ng2-charts';
import { Dataset } from 'src/app/model/Dataset';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;

  @ViewChild('grafico1ID', { static: false })
  grafico1ID: BaseChartDirective = undefined;

  public barChartOptions: any = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            minTicksLimit: 5,
            maxTicksLimit: 8
          }
        }
      ]
    },
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public barChartLabels: string[] = ['2020.1'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: Dataset[] = [
    { data: [147], label: 'Usuários' },
    { data: [113], label: 'Acadêmicos' }
  ];

  constructor(private homeService: HomeService,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    const usuario = this.localStorage.get('usuario');
    this.buscarDados();
  }

  buscarDados() {
    this.blockUI.start();
    this.homeService.buscarDados()
      .subscribe(retorno => {
        let datasetList: Array<Dataset> = [];
        let totalUsuario: number[] = [];
        totalUsuario.push(retorno.totalUsuario);

        let totalAcademico: number[] = [];
        totalAcademico.push(retorno.totalAcademico);

        datasetList.push({ data: totalUsuario, label: 'Usuários' });
        datasetList.push({ data: totalAcademico, label: 'Acadêmicos' });
        this.barChartData = datasetList;

        this.initializeGrafico(this.grafico1ID, this.barChartData, this.barChartOptions);
        this.blockUI.stop();
      });
  }

  private initializeGrafico(grafico: BaseChartDirective, datasets: Array<Dataset>, options: any) {
    grafico.ngOnDestroy();
    grafico.chart = 0;
    grafico.datasets = datasets;
    grafico.options = options;
    grafico.ngOnInit();
  }
}