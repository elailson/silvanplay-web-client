import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastraInstituicaoEducacionalComponent } from './cadastra-instituicao-educacional.component';

describe('CadastraInstituicaoEducacionalComponent', () => {
  let component: CadastraInstituicaoEducacionalComponent;
  let fixture: ComponentFixture<CadastraInstituicaoEducacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastraInstituicaoEducacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastraInstituicaoEducacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
