import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Administrador } from 'src/app/model/Administrador';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-web-storage';
import { ToasterService } from 'angular2-toaster';
import { CadastroAdministradorService } from 'src/app/services/cadastra-administrador.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-cadastra-administrador',
  templateUrl: './cadastra-administrador.component.html',
  styleUrls: ['./cadastra-administrador.component.css']
})
export class CadastraAdministradorComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  administrador: FormGroup;
  objAdministrador: Administrador;

  permissoes = [
    { viewValue: 'Educação', value: 2 },
    { viewValue: 'Gerenciador de Solicitações', value: 5 },
    { viewValue: 'Telefones Úteis', value: 10 }
  ];

  dataSource: MatTableDataSource<Administrador>;
  displayedColumns: string[] = ['usuario', 'permissao', 'editar', 'deletar'];

  acaoBotao = 'Cadastrar';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private toasterService: ToasterService,
    private localStorage: LocalStorageService,
    private cadastraAdministradorService: CadastroAdministradorService) { }

  ngOnInit() {
    this.blockUI.start('Buscando usuários(as)...');
    this.buscaTodosAdministradores();
    this.administrador = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['', Validators.required],
      permissoes: ['', Validators.required]
    });
  }

  cadastraEdita() {
    if (this.validaFormulario()) {
      if (this.acaoBotao === 'Cadastrar') {
        this.blockUI.start('Criando novo usuário...');
        this.cadastraAdministradorService.cadastra(this.objAdministrador)
          .subscribe(retorno => {
            this.toasterService.pop('success', `Novo usuário(a) ${retorno.usuario} cadastrado(a) com sucesso!`);
            this.administrador.reset();
            this.buscaTodosAdministradores();
            this.blockUI.stop();
          }, error => {
            this.toasterService.pop('error', 'Erro ao cadastrar: ' + error);
            this.blockUI.stop();
          });
      } else {
        this.blockUI.start('Alterando dados...');
        this.cadastraAdministradorService.atualizaDados(this.objAdministrador)
          .subscribe(retorno => {
            this.toasterService.pop('success', 'Dados atualizados com sucesso!');
            this.administrador.reset();
            this.buscaTodosAdministradores();
            this.blockUI.stop();
          }, error => {
            this.toasterService.pop('error', 'Erro ao atualizar: ' + error);
            this.blockUI.stop();
          });
      }
    } else {
      this.toasterService.pop('warning', 'Preencha os campos obrigatórios');
    }
  }

  edita(event) {
    this.acaoBotao = 'Alterar';
    this.administrador.patchValue({ usuario: event.usuario });
    this.objAdministrador = new Administrador(event.usuario, '');
    this.objAdministrador.id = event.id;
  }

  deleta(event) {
    this.cadastraAdministradorService.deleta(event.id)
      .subscribe(retorno => {
        this.toasterService.pop('success', 'Administrador removido com sucesso!');
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('error', 'Erro ao remover: ' + error);
        this.blockUI.stop();
      });
      this.buscaTodosAdministradores();
  }

  buscaTodosAdministradores() {
    this.cadastraAdministradorService.buscaTodos()
      .subscribe(retorno => {
        this.dataSource = new MatTableDataSource<Administrador>(retorno);
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('warning', 'Erro ao buscar usuários(as)')
        this.blockUI.stop()
      });
  }

  permissaoToString(permissoes: string): string {
    const listPermissoes = permissoes.split(',');
    let permissoesString = '';
    listPermissoes.forEach(permissao => {
      permissoesString += this.verifica(permissao);
      permissoesString += ', ';
    });

    return permissoesString;
  }

  verifica(permissao: string): string {
    switch (permissao.trim()) {
      case '2':
        return 'Educação';
      case '5':
        return 'Gerenciador de Solicitações';
      case '7':
        return 'Administrador Master';
      case '10':
        return 'Telefones Úteis';
    }
  }

  validaFormulario() {
    if (this.administrador.valid) {
      if (this.acaoBotao === 'Cadastrar') {
        this.objAdministrador = new Administrador(this.administrador.get('usuario').value, this.administrador.get('senha').value);
        this.objAdministrador.permissao = JSON.stringify(this.administrador.get('permissoes').value).replace(/[\[\]']+/g, '');
      } else {
        this.objAdministrador.usuario = this.administrador.get('usuario').value;
        this.objAdministrador.senha = this.administrador.get('senha').value;
        this.objAdministrador.permissao = JSON.stringify(this.administrador.get('permissoes').value).replace(/[\[\]']+/g, '');
      }
      return true;
    }
    return false;
  }

}
