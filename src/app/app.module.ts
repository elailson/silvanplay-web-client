import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BlockUIModule } from 'ng-block-ui';
import { ToasterModule } from 'angular2-toaster';
import { NgxMaskModule } from 'ngx-mask';
import { ChartsModule } from 'ng2-charts';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import 'chartsjs-plugin-data-labels';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { HeaderComponent } from './pages/template/header/header.component';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CadastraInstituicaoEducacionalComponent } from './pages/cadastra-instituicao-educacional/cadastra-instituicao-educacional.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CadastraTelefoneComponent } from './pages/cadastra-telefone/cadastra-telefone.component';
import { GerenciaSolicitacaoComponent } from './pages/gerencia-solicitacao/gerencia-solicitacao.component';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { AuthGuardEducacao } from './auth/guards/auth.guardEducacao';
import { HomeComponent } from './pages/home/home.component';
import { MatSortModule } from '@angular/material/sort';
import { AuthInterceptor } from './auth/interceptors/AuthInterceptor';
import { CadastraAdministradorComponent } from './pages/cadastra-administrador/cadastra-administrador.component';
import { AuthGuardMaster } from './auth/guards/auth.guardMaster';
import { GerenciaAcademicoComponent } from './pages/gerencia-academico/gerencia-academico.component';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material';
import { GerenciaRoteiroComponent } from './pages/gerencia-roteiro/gerencia-roteiro.component';
import { CriarRoteiroDialogComponent } from './dialogs/criar-roteiro/criar-roteiro-dialog.component';
import { DetalhesSolicitacaoDialogComponent } from './dialogs/detalhes-solicitacao/detalhes-solicitacao.component';
import { AuthGuardSolicitacao } from './auth/guards/auth.guardSolicitacao';
import { AuthGuardLogado } from './auth/guards/auth.guardLogado';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CadastraInstituicaoEducacionalComponent,
    LoginComponent,
    CadastraTelefoneComponent,
    GerenciaSolicitacaoComponent,
    GerenciaAcademicoComponent,
    GerenciaRoteiroComponent,
    HomeComponent,
    CriarRoteiroDialogComponent,
    CadastraAdministradorComponent,
    DetalhesSolicitacaoDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    MatPaginatorModule,
    MatOptionModule,
    MatSortModule,
    ChartsModule,
    MatCardModule,
    NgxHmCarouselModule,
    ToasterModule.forRoot(),
    BlockUIModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  providers: [
    AuthGuardEducacao,
    AuthGuardMaster,
    AuthGuardSolicitacao,
    AuthGuardLogado,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  entryComponents: [
    CriarRoteiroDialogComponent,
    DetalhesSolicitacaoDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
