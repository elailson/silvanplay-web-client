import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Roteiro } from '../model/Roteiro';

@Injectable({ providedIn: 'root' })
export class RoteiroService {

    private relativePath: string = environment.url;

    constructor(private http: HttpClient) { }
    
    save(roteiro: Roteiro): Observable<Roteiro> {
        return this.http.post<Roteiro>(this.relativePath + 'roteiro/save', roteiro);
    }

    findAll(): Observable<Array<Roteiro>> {
        return this.http.get<Array<Roteiro>>(this.relativePath + 'roteiro/');
    }

    search(nome: string, campus: string, roteiro: string): Observable<Array<Roteiro>> {
        return this.http.get<Array<Roteiro>>(this.relativePath + 'roteiro/search/' + nome + '/' + campus + '/' + roteiro);
    }

    protected getHeaders() {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return headers;
    }
}