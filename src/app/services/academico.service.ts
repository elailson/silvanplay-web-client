import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Academico } from '../model/Academico';

@Injectable({ providedIn: 'root' })
export class AcademicoService {

    private relativePath: string = environment.url;

    constructor(private http: HttpClient) { }

    findAll(): Observable<Array<Academico>> {
        return this.http.get<Array<Academico>>(this.relativePath + 'academico/');
    }

    protected getHeaders() {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return headers;
    }
}